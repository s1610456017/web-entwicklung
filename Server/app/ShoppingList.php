<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ShoppingList extends Model
{
    protected $fillable = [
        'title', 'description', 'creator_id', 'helper_id',
        'date', 'total_price', 'photo_bill', 'status'
    ];

    public function items() : BelongsToMany {
        return $this->belongsToMany(Item::class, 'item_shopping_list',
            'shopping_list_id', 'item_id')->withPivot('amount', 'max_price')->withTimestamps();
    }

    public function feedback() : HasMany {
        return $this->hasMany(Feedback::class);
    }
}
