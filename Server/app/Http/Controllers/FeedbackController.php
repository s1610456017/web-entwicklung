<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\ShoppingList;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FeedbackController extends Controller
{
    public function index() {
        $feedback = Feedback::get();
        return $feedback;
    }

    /*public function show($list) {
        $list = ShoppingList::find($list);
        return $list;
    }*/

    public function findByID(int $id) : Feedback {
        $feedback = Feedback::where('id', $id)
            ->first();
        return $feedback;
    }

    public function save(Request $request, int $listId) : JsonResponse {
        $request = $this->parseRequest($request);

        DB::beginTransaction();
        try {
            $feedback = Feedback::create($request->all());
            $list = ShoppingList::find($listId);

            $list->feedback()->save($feedback);

            DB::commit();
            return response()->json($feedback, 201);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json("saving feedback failed: " . $e->getMessage(), 420);
        }
    }

    public function update(Request $request, int $id) : JsonResponse
    {
        DB::beginTransaction();
        try {
            $feedback = Feedback::where('id', $id)->first();

            if ($feedback != null) {
                $request = $this->parseRequest($request);
                $feedback->update($request->all());

            }

            DB::commit();
            $feedback1 = Feedback::where('id', $id)->first();
            return response()->json($feedback1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating feedback failed: " . $e->getMessage(), 420);
        }
    }

    public function delete(int $id) : JsonResponse {

        $feedback = Feedback::where('id', $id)->first();
        if ($feedback != null)
            $feedback->delete();
        else
            throw new \Exception("feedback could not be deleted - it does not exist");
        return response()->json('feedback (ID: ' . $feedback['id'] . ') succesfully deleted', 200);
    }

    private function parseRequest(Request $request) : Request {
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}
