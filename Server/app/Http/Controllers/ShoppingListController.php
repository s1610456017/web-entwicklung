<?php

namespace App\Http\Controllers;

use App\Feedback;
use App\Item;
use App\ShoppingList;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ShoppingListController extends Controller
{
    public function index() {
        return ShoppingList::with(['items', 'feedback'])
            ->orderBy('updated_at', 'desc')
            ->get();
    }

    public function show($list) {
        $list = ShoppingList::find($list);
        return $list;
    }

    public function findByID(int $id) : ShoppingList {
        return ShoppingList::where('id', $id)
            ->with(['items', 'feedback'])
            ->first();
    }

    public function findByIDWithUsers(int $id) : ShoppingList {
        $list = ShoppingList::with(['items', 'feedback'])
            ->leftJoin('users as c', 'c.id', 'shopping_lists.creator_id')
            ->leftJoin('users as h', 'h.id', 'shopping_lists.helper_id')
            ->where('shopping_lists.id', $id)
            ->select(
                'shopping_lists.*',
                'c.id as creatorId',
                'c.first_name as creatorFirstName',
                'c.last_name as creatorLastName',
                'c.street as creatorStreet',
                'c.street_number as creatorStreetNumber',
                'c.zip as creatorZip',
                'c.city as creatorCity',
                'c.country as creatorCountry',
                'h.id as helperId',
                'h.first_name as helperFirstName',
                'h.last_name as helperLastName',
            )
            ->first();

        foreach ($list->items as $item) {
            $item->pivot;
        }

        return $list;
    }

    public function getAllListsByStatus(int $status) {
        return ShoppingList::where('status', $status)->with(['items', 'feedback'])
            ->orderBy('updated_at', 'desc')->get();
    }

    public function getListsByUserAndStatus(int $id, int $status) {
        $user = User::find($id);
        if ($user->role == 0)
            $lists = ShoppingList::where('creator_id', $id)->where('status', $status)->with(['items', 'feedback'])
                ->orderBy('updated_at', 'desc')->get();
        if ($user->role == 1)
            $lists = ShoppingList::where('helper_id', $id)->where('status', $status)->with(['items', 'feedback'])
                ->orderBy('updated_at', 'desc')->get();
        return $lists;
    }

    public function getListsByCreator(int $id) {
        return ShoppingList::where('creator_id', $id)->with(['items', 'feedback'])
            ->orderBy('updated_at', 'desc')->get();
    }

    public function getListsByHelper(int $id) {

        return ShoppingList::where('helper_id', $id)->with(['items', 'feedback'])
            ->orderBy('updated_at', 'desc')->get();
    }

    public function save(Request $request) : JsonResponse {
        $request = $this->parseRequest($request);

        DB::beginTransaction();
        try {
            $list = ShoppingList::create($request->all());
            if (isset($request['feedback']) && is_array($request['feedback'])) {
                foreach ($request['feedback'] as $comment) {
                    $feedback = Feedback::firstOrNew(['comment' => $comment['comment'],
                                                        'user_id' => $comment['user_id'],
                                                        'shopping_list_id' => $comment['shopping_list_id']]);
                    $list->feedback()->save($feedback);
                }
            }
            if (isset($request['items']) && is_array($request['items'])) {
                foreach ($request['items'] as $it) {
                    $item = Item::firstOrNew(['title' => $it['title'],
                                              'description' => $it['description'],
                                              'amount' => $it['amount'],
                                              'max_price' => $it['max_price']]);
                    $list->items()->save($item);
                }
            }
            DB::commit();
            return response()->json($list, 201);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json("saving list failed: " . $e->getMessage(), 420);
        }
    }

    public function updateHelper(Request $request, int $id) {
        DB::beginTransaction();
        try {

            $request = $this->parseRequest($request);

            $list = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();

            if ($list != null) {
                $list['helper_id'] = $request->helper_id;
                $list['status'] = 1;
                $list->save();
            }

            DB::commit();
            $list1 = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();
            return response()->json($list1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating list failed: " . $e->getMessage(), 420);
        }
    }

    /*public function updateStatus(Request $request, int $id) {
        DB::beginTransaction();
        try {
            $list = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();

            if ($list != null) {
                $list['status'] = 2;
                $list->save();
            }

            DB::commit();
            $list1 = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();
            return response()->json($list1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating list failed: " . $e->getMessage(), 420);
        }
    }*/

    public function updatePrice(Request $request, int $id) {
        DB::beginTransaction();
        try {

            $request = $this->parseRequest($request);
            $list = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();

            if ($list != null) {
                $list['total_price'] = $request->total_price;
                $list['photo_bill'] = $request->photo_bill;
                $list['status'] = 2;
                $list->save();
            }

            DB::commit();
            $list1 = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();
            return response()->json($list1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating list failed: " . $e->getMessage(), 420);
        }
    }

    public function updateInfo(Request $request, int $id) {
        DB::beginTransaction();
        try {

            $request = $this->parseRequest($request);
            $list = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();

            if ($list != null) {
                $list['title'] = $request->title;
                $list['description'] = $request->description;
                $list['date'] = $request->date;
                $list->save();
            }

            DB::commit();
            $list1 = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();
            return response()->json($list1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating list failed: " . $e->getMessage(), 420);
        }
    }

    public function update(Request $request, int $id) : JsonResponse
    {
        DB::beginTransaction();
        try {
            $list = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();

            if ($list != null) {
                $request = $this->parseRequest($request);
                if ($list['status'] == 0) {
                    $list->update($request->all());
                    $list->save();
                } else {
                    return response()->json("list " . $list['title'] . " can not be edited anymore, because someone is helping already.");
                }
            }

            DB::commit();
            $list1 = ShoppingList::with(['items', 'feedback'])
                ->where('id', $id)->first();
            return response()->json($list1, 201);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating list failed: " . $e->getMessage(), 420);
        }
    }

    public function addItem(Request $request, int $id) : JsonResponse {

        DB::beginTransaction();
        try {
            $list = ShoppingList::with(['items'])
                ->where('id', $id)->first();

            if ($list != null && $list['status'] == 0) {
                $request = $this->parseRequest($request);

                $ids = [];
                foreach ($list['items'] as $item) {
                    array_push($ids, $item['id']);
                }

                $item = Item::create($request->all());
                if ($item) {
                    array_push($ids, $item['id']);
                }

                $list->items()->sync($ids);
                $list->items()->attach($item['id'], ['max_price' => $item['max_price'], 'amount' => $item['amount']]);

                $list->save();

                DB::commit();
                return response()->json($list, 201);
            } else {
                DB::rollBack();
                return response()->json("list " . $list['title'] . " can not be edited anymore, because someone is helping already.");
            }
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json("adding item to list failed: " . $e->getMessage(), 420);
        }
    }

    public function delete(int $id) : JsonResponse {

        $list = ShoppingList::where('id', $id)->first();
        if ($list != null)
            $list->delete();
        else
            throw new \Exception("list could not be deleted - it does not exist");
        return response()->json('list (' . $list['title'] . ') successfully deleted', 200);
    }

    private function parseRequest(Request $request) : Request {
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}
