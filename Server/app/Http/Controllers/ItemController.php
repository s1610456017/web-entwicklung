<?php

namespace App\Http\Controllers;

use App\Item;
use App\ShoppingList;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ItemController extends Controller
{
    public function index() {
        $items = Item::get();
        return $items;
    }

    public function getAllItemsInList(int $listId) {

        $list = ShoppingList::with('items')->find($listId);
        $items = $list->items;
        foreach ($items as $item) {
            $item->pivot;
        }
        return $items;
    }

    //TODO weg?
    public function getTotalMaxPrice(int $listId) : float {
        $price = 0;
        $list = ShoppingList::with('items')->find($listId);
        $items = $list->items;
        foreach ($items as $item) {
            $price += $item->pivot->max_price;
        }
        return $price;
    }

    public function findByID(int $id) : Item {
        $item = Item::where('id', $id)
            ->first();
        return $item;
    }

    public function save(Request $request, int $listId) : JsonResponse {
        $request = $this->parseRequest($request);

        DB::beginTransaction();
        try {

            $item = Item::create($request->all());
            $list = ShoppingList::find($listId);

            $list->items()->attach($item->id, ['amount' => $request->amount, 'max_price' => $request->max_price]);

            DB::commit();

            return response()->json($item, 201);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json("saving item failed: " . $e->getMessage(), 420);
        }
    }

    public function update(Request $request, int $id) : JsonResponse
    {
        DB::beginTransaction();
        try {
            $item = Item::where('id', $id)->first();

            if ($item != null) {
                $request = $this->parseRequest($request);
                $item->update($request->all());

            }

            DB::commit();
            $item1 = Item::where('id', $id)->first();
            return response()->json($item1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating item failed: " . $e->getMessage(), 420);
        }
    }

    public function delete(int $id) : JsonResponse {

        $item = Item::where('id', $id)->first();
        if ($item != null)
            $item->delete();
        else
            throw new \Exception("item could not be deleted - it does not exist");
        return response()->json('item (ID: ' . $item['id'] . ') successfully deleted', 200);
    }

    private function parseRequest(Request $request) : Request {
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}
