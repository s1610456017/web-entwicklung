<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{
    public function index() {
        $users = User::get();
        return $users;
    }

    /*public function show($list) {
        $list = ShoppingList::find($list);
        return $list;
    }*/

    public function findByID(int $id) : User {
        $user = User::where('id', $id)
            ->first();
        return $user;
    }

    /*
        public function getListsByStatus(int $status) {
            $lists = ShoppingList::where('status', $status)->with(['items', 'feedback'])->get();
            return $lists;
        }

        public function getListsByCreator(int $id) {
            $lists = ShoppingList::where('creator_id', $id)->with(['items', 'feedback'])->get();
            return $lists;
        }

        public function getListsByHelper(int $id) {
            $lists = ShoppingList::where('helper_id', $id)->with(['items', 'feedback'])->get();
            return $lists;
        }
    */
    public function save(Request $request) : JsonResponse {
        $request = $this->parseRequest($request);

        DB::beginTransaction();
        try {
            $user = User::create($request->all());

            DB::commit();
            return response()->json($user, 201);
        }
        catch (\Exception $e) {
            DB::rollBack();
            return response()->json("saving user failed: " . $e->getMessage(), 420);
        }
    }

    public function update(Request $request, int $id) : JsonResponse
    {
        DB::beginTransaction();
        try {
            $user = User::where('id', $id)->first();

            if ($user != null) {
                $request = $this->parseRequest($request);
                $user->update($request->all());

            }

            DB::commit();
            $user1 = User::where('id', $id)->first();
            return response()->json($user1, 201);
        } catch (\Exception $e) {
            DB::rollBack();
            return response()->json("updating user failed: " . $e->getMessage(), 420);
        }
    }

        public function delete(int $id) : JsonResponse {

            $user = User::where('id', $id)->first();
            if ($user != null)
                $user->delete();
            else
                throw new \Exception("user could not be deleted - this person does not exist");
            return response()->json('user (ID: ' . $user['id'] . ') succesfully deleted', 200);
        }

    private function parseRequest(Request $request) : Request {
        $date = new \DateTime($request->date);
        $request['date'] = $date;
        return $request;
    }
}
