<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Item extends Model
{
    protected $fillable = [
        'title', 'description'
    ];

    public function shoppinglists() : BelongsToMany {
        return $this->belongsToMany(ShoppingList::class, 'item_shopping_list',
            'item_id', 'shopping_list_id')->withPivot('amount', 'max_price')->withTimestamps();
    }

}
