<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Feedback extends Model
{
    protected $fillable = [
        'comment', 'user_id', 'shopping_list_id'
    ];

    public function shoppinglist() : BelongsTo {
        return $this->belongsTo(ShoppingList::class);
    }
}
