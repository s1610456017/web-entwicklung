<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemShoppingListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_shopping_list', function (Blueprint $table) {
            $table->bigInteger('shopping_list_id')->unsigned()->index();
            $table->foreign('shopping_list_id')
                ->references('id')->on('shopping_lists')
                ->onDelete('cascade');

            $table->bigInteger('item_id')->unsigned()->index();
            $table->foreign('item_id')
                ->references('id')->on('items');

            $table->string('amount')->default(1);
            $table->float('max_price')->nullable();

            $table->primary(['shopping_list_id', 'item_id']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item_shopping_list');
    }
}
