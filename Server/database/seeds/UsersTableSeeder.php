<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user1 = new App\User;
        $user1->first_name = 'Anna';
        $user1->last_name = 'Winter';
        $user1->email = 'anna.winter@test.at';
        $user1->password = bcrypt('secret');
        $user1->street = 'Leitenbauerstraße';
        $user1->street_number = '7';
        $user1->zip = 4040;
        $user1->city = 'Linz';
        $user1->country = 'Österreich';
        $user1->role = 0;
        $user1->save();

        $user2 = new App\User;
        $user2->first_name = 'Christian';
        $user2->last_name = 'Gröber';
        $user2->email = 'christian.groeber@test.at';
        $user2->password = bcrypt('secret');
        $user2->street = 'Wernickestraße';
        $user2->street_number = '4';
        $user2->zip = 4040;
        $user2->city = 'Linz';
        $user2->country = 'Österreich';
        $user2->role = 0;
        $user2->save();

        $user3 = new App\User;
        $user3->first_name = 'Sina';
        $user3->last_name = 'Berger';
        $user3->email = 'sina.berger@test.at';
        $user3->password = bcrypt('secret');
        /*$user3->street = 'Wolfauerstraße';
        $user3->street_number = '28';
        $user3->zip = 4040;
        $user3->city = 'Linz';
        $user3->country = 'Österreich';*/
        $user3->role = 1;
        $user3->save();

        $user4 = new App\User;
        $user4->first_name = 'Thomas';
        $user4->last_name = 'Wagner';
        $user4->email = 'thomas.wagner@test.at';
        $user4->password = bcrypt('secret');
        /*$user4->street = 'Wolfauerstraße';
        $user4->street_number = '28';
        $user4->zip = 4040;
        $user4->city = 'Linz';
        $user4->country = 'Österreich';*/
        $user4->role = 1;
        $user4->save();
    }
}
