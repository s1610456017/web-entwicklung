<?php

use Illuminate\Database\Seeder;

class FeedbackTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $feedback1 = new App\Feedback;
        $feedback1->comment = 'comment1';
        $feedback1->user_id = '1';
        $feedback1->shopping_list_id = '1';
        $feedback1->save();

        $feedback2 = new App\Feedback;
        $feedback2->comment = 'comment2';
        $feedback2->user_id = '4';
        $feedback2->shopping_list_id = '6';
        $feedback2->save();

        $feedback3 = new App\Feedback;
        $feedback3->comment = 'comment3';
        $feedback3->user_id = '2';
        $feedback3->shopping_list_id = '6';
        $feedback3->save();

        $feedback4 = new App\Feedback;
        $feedback4->comment = 'comment4';
        $feedback4->user_id = '4';
        $feedback4->shopping_list_id = '6';
        $feedback4->save();
    }
}
