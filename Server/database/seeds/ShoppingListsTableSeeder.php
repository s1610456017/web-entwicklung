<?php

use Illuminate\Database\Seeder;

class ShoppingListsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list1 = new App\ShoppingList;
        $list1->title = 'List1';
        $list1->description = 'List1 Beschreibung';
        $list1->creator_id = '1';
        //$list1->helper_id;
        $list1->date = new DateTime();
        //$list1->total_price;
        //$list1->photo_bill;
        $list1->status = '0';
        $list1->save();

        $items = App\Item::all()->pluck('id');
        $list1->items()->sync($items);
        $list1->save();


        $list2 = new App\ShoppingList;
        $list2->title = 'List2';
        $list2->description = 'List2 Beschreibung';
        $list2->creator_id = '1';
        $list2->helper_id = '3';
        $list2->date = new DateTime();
        //$list2->total_price;
        //$list2->photo_bill;
        $list2->status = '1';
        $list2->save();

        $list3 = new App\ShoppingList;
        $list3->title = 'List3';
        $list3->description = 'List3 Beschreibung';
        $list3->creator_id = '1';
        $list3->helper_id = '4';
        $list3->date = new DateTime();
        $list3->total_price = 30.74;
        //$list3->photo_bill;
        $list3->status = '2';
        $list3->save();

        $list4 = new App\ShoppingList;
        $list4->title = 'List4';
        $list4->description = 'List4 Beschreibung';
        $list4->creator_id = '2';
        //$list4->helper_id;
        $list4->date = new DateTime();
        //$list4->total_price;
        //$list4->photo_bill;
        $list4->status = '0';
        $list4->save();

        $list5 = new App\ShoppingList;
        $list5->title = 'List5';
        $list5->description = 'List5 Beschreibung';
        $list5->creator_id = '2';
        //$list5->helper_id;
        $list5->date = new DateTime();
        //$list5->total_price;
        //$list5->photo_bill;
        $list5->status = '0';
        $list5->save();

        $list6 = new App\ShoppingList;
        $list6->title = 'List6';
        $list6->description = 'List6 Beschreibung';
        $list6->creator_id = '2';
        $list6->helper_id = '4';
        $list6->date = new DateTime();
        //$list6->total_price;
        //$list6->photo_bill;
        $list6->status = '1';
        $list6->save();
    }
}
