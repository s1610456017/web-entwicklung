<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item1 = new App\Item;
        $item1->title = 'Item1';
        $item1->description = 'Item1 Beschreibung';

        /*$lists = App\ShoppingList::all()->pluck('id');
        $item1->shoppinglists()->sync($lists);*/
        $item1->save();

        $item2 = new App\Item;
        $item2->title = 'Item2';
        $item2->description = 'Item2 Beschreibung';
        $item2->save();

        $item3 = new App\Item;
        $item3->title = 'Item3';
        $item3->description = 'Item3 Beschreibung';
        $item3->save();

        $item4 = new App\Item;
        $item4->title = 'Item4';
        $item4->description = 'Item4 Beschreibung';
        $item4->save();

        $item5 = new App\Item;
        $item5->title = 'Item5';
        $item5->description = 'Item5 Beschreibung';
        $item5->save();

        $item6 = new App\Item;
        $item6->title = 'Item6';
        $item6->description = 'Item6 Beschreibung';
        $item6->save();

        $item7 = new App\Item;
        $item7->title = 'Item7';
        $item7->description = 'Item7 Beschreibung';
        $item7->save();

        $item8 = new App\Item;
        $item8->title = 'Item8';
        $item8->description = 'Item8 Beschreibung';
        $item8->save();
    }
}
