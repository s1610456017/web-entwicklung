<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'cors']], function() {
    Route::get('lists', 'ShoppingListController@index');
    Route::get('list/{id}', 'ShoppingListController@findByID');
    Route::get('list-with-user-data/{id}', 'ShoppingListController@findByIDWithUsers');

    Route::put('list/{id}/helper', 'ShoppingListController@updateHelper');
    Route::put('list/{id}/status', 'ShoppingListController@updateStatus');
    Route::put('list/{id}/price', 'ShoppingListController@updatePrice');
    Route::put('list/{id}/edit', 'ShoppingListController@updateInfo');

    Route::post('list-add-item/{id}', 'ShoppingListController@update');
    Route::get('lists/{status}', 'ShoppingListController@getAllListsByStatus');
    Route::get('creator-lists/{userId}', 'ShoppingListController@getListsByCreator');
    Route::get('helper-lists/{userId}', 'ShoppingListController@getListsByHelper');
    Route::get('lists/{userId}/{status}', 'ShoppingListController@getListsByUserAndStatus');
    Route::post('list', 'ShoppingListController@save');
    Route::put('list/{id}', 'ShoppingListController@update');
    Route::delete('list/{id}', 'ShoppingListController@delete');

    Route::get('users', 'UserController@index');
    Route::get('user/{id}', 'UserController@findByID');
    Route::post('user', 'UserController@save');
    Route::put('user/{id}', 'UserController@update');
    Route::delete('user/{id}', 'UserController@delete');

    Route::get('feedback', 'FeedbackController@index');
    Route::get('lists/{listId}/feedback', 'FeedbackController@getAllsFeedbackInList');
    Route::get('feedback/{id}', 'FeedbackController@findByID');
    Route::post('feedback', 'FeedbackController@save');
    Route::post('lists/{listId}/feedback', 'FeedbackController@save');
    Route::put('feedback/{id}', 'FeedbackController@update');
    Route::delete('feedback/{id}', 'FeedbackController@delete');

    Route::get('items', 'ItemController@index');
    Route::get('list/{listId}/items', 'ItemController@getAllItemsInList');
    Route::get('list/{listId}/items/price', 'ItemController@getMaxTotalPrice');
    Route::get('item/{id}', 'ItemController@findByID');
    Route::post('list/{listId}/item', 'ItemController@save');
    Route::put('item/{id}', 'ItemController@update');
    Route::delete('item/{id}', 'ItemController@delete');

    Route::post('auth/logout', 'Auth\ApiAuthController@logout');
    Route::post('auth/login', 'Auth\ApiAuthController@login');
});
