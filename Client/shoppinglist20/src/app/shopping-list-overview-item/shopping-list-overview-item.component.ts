import { Component, OnInit, Input } from '@angular/core';
import {ShoppingList} from "../shared/shopping-list";

@Component({
  selector: 'span.bs-shopping-list-overview-item',
  templateUrl: './shopping-list-overview-item.component.html',
  styles: []
})
export class ShoppingListOverviewItemComponent implements OnInit {

  @Input() shoppingList: ShoppingList

  constructor() { }

  ngOnInit() {
  }
}
