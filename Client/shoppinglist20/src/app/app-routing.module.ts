import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";
import {ShoppingListDetailComponent} from "./shopping-list-detail/shopping-list-detail.component";
import {ShoppingListOverviewComponent} from "./shopping-list-overview/shopping-list-overview.component";
import {LoginComponent} from "./login/login.component";
import {ShoppingListFormComponent} from "./shopping-list-form/shopping-list-form.component";
import {AccountComponent} from "./account/account.component";
import {ItemFormComponent} from "./item-form/item-form.component";
import {HomeComponent} from "./home/home.component";

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'lists', component: ShoppingListOverviewComponent },
  { path: 'lists/:listId', component: ShoppingListDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'newlist', component: ShoppingListFormComponent },
  { path: 'edit/:listId', component: ShoppingListFormComponent },
  { path: 'account', component: AccountComponent },
  { path: 'list/:listId/newitem', component: ItemFormComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule {}
