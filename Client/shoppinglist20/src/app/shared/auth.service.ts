import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {isNullOrUndefined} from "util";
import jwt_decode from "jwt-decode";
import {retry} from "rxjs/operators";
import {Router} from "@angular/router";

interface User {
  result: {
    created_at: Date,
    email: string,
    id: number,
    firstName: string,
    lastName: string,
    role: number,
    updated_at: Date
  }
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private api = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api/auth';

  constructor(
    private http: HttpClient,
    private router: Router
    ) { }

  login(email: string, password: string) {
    return this.http.post(`${this.api}/login`, {'email': email, 'password': password});
  }

  public getCurrentUserId() {
    return Number.parseInt(localStorage.getItem('userId'));
  }

  public setCurrentUserId() {
    return this.http.get<User>(`${this.api}/user`).pipe(retry(3)).subscribe(res => {
      localStorage.setItem('userId', res.result.id.toString());
    })
  }

  public getCurrentUserRole() {
    return Number.parseInt(localStorage.getItem('role'));
  }

  public getCurrentUserFirstName() {
    return localStorage.getItem('firstName');
  }

  public getCurrentUserLastName() {
    return localStorage.getItem('lastName');
  }

  public getCurrentUserAddress() {
    return localStorage.getItem('street');
  }

  public setLocalStorage(token: string) {
    const decodedToken = jwt_decode(token);

    localStorage.setItem('token', token);
    localStorage.setItem('userId', decodedToken.user.id);
    localStorage.setItem('role', decodedToken.user.role);
    localStorage.setItem('firstName', decodedToken.user.first_name);
    localStorage.setItem('lastName', decodedToken.user.last_name);
    localStorage.setItem('street', decodedToken.user.street);
    localStorage.setItem('streetNumber', decodedToken.user.street_number);
    localStorage.setItem('zip', decodedToken.user.zip);
    localStorage.setItem('city', decodedToken.user.city);
    localStorage.setItem('country', decodedToken.user.country);
  }

  logout() {
    this.http.post(`${this.api}/logout`, {});
    localStorage.clear();
    this.router.navigate(['./'])
  }

  public isLoggedIn() {
    if (!isNullOrUndefined(localStorage.getItem('token'))) {
      const token : string = localStorage.getItem('token');
      const decodedToken = jwt_decode(token);
      let expirationDate: Date = new Date(0);
      expirationDate.setUTCSeconds(decodedToken.exp);
      if (expirationDate < new Date()) {
        console.log("token expired");
        localStorage.removeItem("token");
        return false;
      }
    } else {
      return false
    }

    return true;
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }

}
