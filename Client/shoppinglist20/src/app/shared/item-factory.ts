import {Item} from "./item";

export class ItemFactory {
  static empty(): Item {
    return new Item(null, null, null, null, null);
  }

  static fromObject(rawItem: any) : Item {
    return new Item(
      rawItem.id,
      rawItem.title,
      rawItem.description,
      rawItem.max_price,
      rawItem.amount
    );

  }
}
