export class User {
  constructor (public id: number,
               public firstName: string,
               public lastName: string,
               public email: string,
               public role: number,
               public street?: string,
               public streetNumber?: string,
               public zip?: number,
               public city?: string,
               public country?: string) {}
}
