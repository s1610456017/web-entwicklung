import {Item} from "./item";
import {Feedback} from "./feedback";

export class ShoppingList {

  constructor (public id: number,
               public title: string,
               public creator_id: number,
               public date: Date,
               public status: number,
               public description?: string,
               public items?: Item[],
               public feedback?: Feedback[],
               public helper_id?: number,
               public total_price?: number,
               public photo_bill?: string,
               public creatorFirstName?: string,
               public creatorLastName?: string,
               public helperFirstName?: string,
               public helperLastName?: string,
               public creatorStreet?: string,
               public creatorStreetNumber?: string,
               public creatorZip?: number,
               public creatorCity?: string,
               public creatorCountry?: string,
               public created_at?: Date,
               public updated_at?: Date
               ) {}
}
