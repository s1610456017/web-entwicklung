import {Feedback} from "./feedback";

export class FeedbackFactory {
  static empty(): Feedback {
    return new Feedback(null, null, null, null);
  }

  static fromObject(rawFeedback: any) : Feedback {
    return new Feedback(
      rawFeedback.id,
      rawFeedback.comment,
      rawFeedback.user_id,
      rawFeedback.shopping_list_id
    );

  }
}
