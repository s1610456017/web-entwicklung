import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {ShoppingList} from "./shopping-list";
import {catchError, retry} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ShoppingListService {

  private api = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api';

  constructor(private http: HttpClient) { }

  getShoppingListsByCreator(creatorId): Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/creator-lists/${creatorId}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  getShoppingListsByHelper(helperId): Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/helper-lists/${helperId}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  getShoppingListsByStatus(status): Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/lists/${status}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  getShoppingListsByUserAndStatus(userId, status): Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/lists/${userId}/${status}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  getAllShoppingLists(): Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/lists`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  getAllShoppingListsByIdWithUsers(id): Observable<ShoppingList> {
    return this.http.get(`${this.api}/list-with-user-data/${id}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  getShoppingListById(id): Observable<ShoppingList> {
    return this.http.get(`${this.api}/list/${id}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  create(shoppingList: ShoppingList) : Observable<any> {
    return this.http.post(`${this.api}/list`, shoppingList)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  updateHelper(shoppingList: ShoppingList) : Observable<any> {
    return this.http.put(`${this.api}/list/${shoppingList.id}/helper`, shoppingList)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  updatePrice(shoppingList: ShoppingList) : Observable<any> {
    return this.http.put(`${this.api}/list/${shoppingList.id}/price`, shoppingList)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  updateInfo(shoppingList: ShoppingList) : Observable<any> {
    return this.http.put(`${this.api}/list/${shoppingList.id}/edit`, shoppingList)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  private errorHandler(error: Error | any) : Observable<any> {
    return throwError(error);
  }
}
