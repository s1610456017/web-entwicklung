import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, retry} from "rxjs/operators";
import {Feedback} from "./feedback";
import {Item} from "./item";

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {
  private api = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api';

  constructor(private http: HttpClient) { }

  getAllFeedbackInList(listId): Observable<Array<Feedback>> {
    return this.http.get(`${this.api}/lists/${listId}/feedback`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  create(comment: Feedback, listId): Observable<any> {
    return this.http.post(`${this.api}/lists/${listId}/feedback`, comment)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: Error | any) : Observable<any> {
    return throwError(error);
  }
}
