import { Injectable } from '@angular/core';
import {Observable, throwError} from "rxjs";
import {ShoppingList} from "./shopping-list";
import {catchError, retry} from "rxjs/operators";
import {HttpClient} from "@angular/common/http";
import {User} from "./user";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private api = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api';

  constructor(private http: HttpClient) { }

  getUserById(id): Observable<User> {
    return this.http.get(`${this.api}/user/${id}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: Error | any) : Observable<any> {
    return throwError(error);
  }
}
