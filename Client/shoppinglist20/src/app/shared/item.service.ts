import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, retry} from "rxjs/operators";
import {Item} from "./item";

@Injectable({
  providedIn: 'root'
})
export class ItemService {

  private api = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api';

  constructor(private http: HttpClient) { }

  getAllItemsInList(listId): Observable<Array<Item>> {
    return this.http.get(`${this.api}/list/${listId}/items`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  //TODO weg?
  getMaxTotalPrice(listId): Observable<any> {
    return this.http.get(`${this.api}/list/${listId}/items/price`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  create(item: Item, listId): Observable<any> {
    return this.http.post(`${this.api}/list/${listId}/item`, item)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  update(item: Item): Observable<any> {
    return this.http.put(`${this.api}/item/${item.id}`, item)
      .pipe(retry(3)).pipe(catchError(this.errorHandler));
  }

  private errorHandler(error: Error | any) : Observable<any> {
    return throwError(error);
  }
}
