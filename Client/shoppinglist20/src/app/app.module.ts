import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {ShoppingListOverviewComponent} from './shopping-list-overview/shopping-list-overview.component';
import {ShoppingListOverviewItemComponent} from './shopping-list-overview-item/shopping-list-overview-item.component';
import {ShoppingListDetailComponent} from './shopping-list-detail/shopping-list-detail.component';
import {ShoppingListService} from "./shared/shopping-list.service";
import {HttpClientModule} from "@angular/common/http";
import {AppRoutingModule} from "./app-routing.module";
import {LoginComponent} from './login/login.component';
import {UserService} from "./shared/user.service";
import {AuthService} from "./shared/auth.service";
import {ReactiveFormsModule} from "@angular/forms";
import {ShoppingListFormComponent} from "./shopping-list-form/shopping-list-form.component";
import {AccountComponent} from './account/account.component';
import {ItemFormComponent} from './item-form/item-form.component';
import {ItemService} from "./shared/item.service";
import { CommentFormComponent } from './comment-form/comment-form.component';
import {FeedbackService} from "./shared/feedback.service";
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    ShoppingListOverviewComponent,
    ShoppingListOverviewItemComponent,
    ShoppingListDetailComponent,
    LoginComponent,
    ShoppingListFormComponent,
    AccountComponent,
    ItemFormComponent,
    CommentFormComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [
    ShoppingListService,
    ItemService,
    UserService,
    FeedbackService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
