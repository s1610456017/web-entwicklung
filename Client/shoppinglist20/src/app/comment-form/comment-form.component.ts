import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ShoppingListFactory} from "../shared/shopping-list-factory";
import {FeedbackFactory} from "../shared/feedback-factory";
import {ActivatedRoute, Router} from "@angular/router";
import {ShoppingListService} from "../shared/shopping-list.service";
import {AuthService} from "../shared/auth.service";
import {Feedback} from "../shared/feedback";
import {FeedbackService} from "../shared/feedback.service";

@Component({
  selector: 'bs-comment-form',
  templateUrl: './comment-form.component.html',
  styles: []
})
export class CommentFormComponent implements OnInit {

  commentForm: FormGroup;
  isUpdatingShoppingList = false;
  shoppingList = ShoppingListFactory.empty();
  comment = FeedbackFactory.empty();

  constructor(private sls: ShoppingListService,
              private as: AuthService,
              private fs: FeedbackService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit() {
    if (this.as.isLoggedIn()) {
      const id = this.route.snapshot.params['listId'];
      this.sls.getAllShoppingListsByIdWithUsers(id).subscribe(shoppingList => {this.shoppingList = shoppingList;});

      this.initComment();
    }
  }

  initComment() {
    this.commentForm = this.fb.group({
      id: this.comment.id,
      comment: this.comment.comment,
      user_id: this.as.getCurrentUserId(),
      shopping_list_id: this.route.snapshot.params['listId']
    });
    //this.commentForm.statusChanges.subscribe();
  }

  submitForm() {
    console.log("kommentar speichern aufgerufen");
    console.log(this.commentForm.value);
    const comment: Feedback = FeedbackFactory.fromObject(this.commentForm.value);
    console.log(comment);

      this.fs.create(comment, this.shoppingList.id).subscribe(res => {
        //this.shoppingList = ShoppingListFactory.empty();
        //this.shoppingListForm.reset(ShoppingListFactory.empty());

        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['/lists', this.route.snapshot.params['listId']]);

      });
  }

}
