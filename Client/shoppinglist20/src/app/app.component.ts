import { Component } from '@angular/core';
import {AuthService} from "./shared/auth.service";

@Component({
  selector: 'bs-root',
  templateUrl: './app.component.html',
  styles: []
})
export class AppComponent {
  title = 'shoppinglist20';

  constructor(private as: AuthService) {
  }

  isLoggedIn() {
    return this.as.isLoggedIn();
  }

  getCurrentUserId() {
    return this.as.getCurrentUserId();
  }

  getCurrentUserRole() {
    return this.as.getCurrentUserRole();
  }
}

