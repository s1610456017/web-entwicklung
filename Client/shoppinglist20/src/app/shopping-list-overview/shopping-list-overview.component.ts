import { Component, OnInit } from '@angular/core';
import {ShoppingListService} from "../shared/shopping-list.service";
import {ShoppingList} from "../shared/shopping-list";
import {AuthService} from "../shared/auth.service";
import {UserService} from "../shared/user.service";
import {ActivatedRoute, Route} from "@angular/router";

declare var $:any;

@Component({
  selector: 'bs-shopping-list-overview',
  templateUrl: './shopping-list-overview.component.html',
  styles: []
})
export class ShoppingListOverviewComponent implements OnInit {

  shoppingLists: ShoppingList[];
  userId = localStorage['userId'];
  firstName = localStorage['firstName'];

  constructor(private sls: ShoppingListService,
              private as: AuthService,
              private route: ActivatedRoute) { }

  ngOnInit() {
    if (this.as.getCurrentUserRole()==0) {
      this.sls.getShoppingListsByCreator(this.as.getCurrentUserId()).subscribe(res => this.shoppingLists = res);
    }
    if (this.as.getCurrentUserRole()==1) {
      this.sls.getShoppingListsByHelper(this.as.getCurrentUserId()).subscribe(res => this.shoppingLists = res);
    }
    //this.sls.getAllShoppingLists().subscribe(res => this.shoppingLists = res);
  }

  showAllOpenLists() {
    this.sls.getShoppingListsByStatus(0).subscribe(res => this.shoppingLists = res);
  }

  showMyLists() {
    this.ngOnInit();
  }

  showMyOpenLists() {
    this.sls.getShoppingListsByUserAndStatus(this.as.getCurrentUserId(), 0)
      .subscribe(res => this.shoppingLists = res);
  }

  showMyPendingLists() {
    this.sls.getShoppingListsByUserAndStatus(this.as.getCurrentUserId(), 1)
      .subscribe(res => this.shoppingLists = res);
  }

  showMyDoneLists() {
    this.sls.getShoppingListsByUserAndStatus(this.as.getCurrentUserId(), 2)
      .subscribe(res => this.shoppingLists = res);
  }

  isLoggedIn() {
    return this.as.isLoggedIn();
  }

  getCurrentUserRole() {
    return this.as.getCurrentUserRole();
  }

  showNewList(): void {
    $('.ui.modal.newlist')
      .modal('show')
    ;
  }

}
