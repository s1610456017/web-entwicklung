import { Component, OnInit } from '@angular/core';
import {AuthService} from "../shared/auth.service";

declare var $:any;

@Component({
  selector: 'bs-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {

  firstName: string
  lastName: string
  role: number
  street: string
  streetNumber: string
  zip: number
  city: string
  country: string

  constructor(private as: AuthService) { }

  ngOnInit() {
      this.firstName = localStorage['firstName'];
      this.lastName = localStorage['lastName'];
      this.role = localStorage['role'];
      this.street = localStorage['street'];
      this.streetNumber = localStorage['streetNumber'];
      this.zip = localStorage['zip'];
      this.city = localStorage['city'];
      this.country = localStorage['country'];
  }

  isLoggedIn() {
    return this.as.isLoggedIn();
  }

  showLogin(): void {
    console.log("login aufgerufen");
    $('.ui.modal.login')
      .modal('show')
    ;
  }

}
