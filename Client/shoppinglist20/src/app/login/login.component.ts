import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthService} from "../shared/auth.service";

interface Response {
  response: string;
  result: {
    token:string;
  }
}

@Component({
  selector: 'bs-login',
  templateUrl: './login.component.html',
  styles: []
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private as: AuthService)
  {}

  ngOnInit() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
    });
  }

  login() {
    const val = this.loginForm.value;
    if (val.username && val.password) {
      this.as.login(val.username, val.password).subscribe(res => {
        const resObj = res as Response;
        if (resObj.response === "success") {
          this.as.setLocalStorage(resObj.result.token);
          this.router.navigate(['./lists']);
        }
      });
    }
  }

  isLoggedIn() {
    return this.as.isLoggedIn();
  }

  logout() {
    this.as.logout();
  }

  getCurrentUserId() {
    return this.as.getCurrentUserId();
  }

  getCurrentUserRole() {
    return this.as.getCurrentUserRole();
  }
}
