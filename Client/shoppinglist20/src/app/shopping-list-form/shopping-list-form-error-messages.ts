export class ErrorMessage {
  constructor(public forControl: string, public forValidator: string, public text: string) {  }
}

export const ShoppingListFormErrorMessages = [
  new ErrorMessage('title', 'required',
    'Die Einkaufliste muss einen Namen haben.'),
  new ErrorMessage('date', 'required',
    'Die Einkaufsliste muss ein Fälligkeitsdatum haben.')
];
