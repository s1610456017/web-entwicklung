import { Component, OnInit } from '@angular/core';
import {ShoppingListService} from "../shared/shopping-list.service";
import {AuthService} from "../shared/auth.service";
import {FormBuilder, FormGroup} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {ShoppingListFactory} from "../shared/shopping-list-factory";
import {ShoppingList} from "../shared/shopping-list";
import {ShoppingListFormErrorMessages} from "./shopping-list-form-error-messages";

@Component({
  selector: 'bs-shopping-list-form',
  templateUrl: './shopping-list-form.component.html',
  styles: []
})
export class ShoppingListFormComponent implements OnInit {

  shoppingListForm: FormGroup;
  shoppingList = ShoppingListFactory.empty();
  errors: { [key: string]: string } = {};
  isUpdatingShoppingList = false;

  constructor(private sls: ShoppingListService,
              private as: AuthService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    if (this.as.isLoggedIn()) {
      const id = this.route.snapshot.params['listId'];
      if (id) {
        this.isUpdatingShoppingList = true;
        this.sls.getShoppingListById(id).subscribe(shoppingList => {
          this.shoppingList = shoppingList;
          this.initShoppingList();
        });
      }
      this.initShoppingList();
    }
    else {
      this.router.navigate(['../'], {relativeTo: this.route});
    }
  }

  initShoppingList() {
    this.shoppingListForm = this.fb.group({
      id: this.shoppingList.id,
      title: this.shoppingList.title,
      description: this.shoppingList.description,
      creator_id: this.as.getCurrentUserId(),
      helper_id: this.shoppingList.helper_id,
      date: this.shoppingList.date,
      total_price: this.shoppingList.total_price,
      photo_bill: this.shoppingList.photo_bill,
      status: this.shoppingList.status,
      items: this.shoppingList.items
    });
    this.shoppingListForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  submitForm() {
    const shoppingList: ShoppingList = ShoppingListFactory.fromObject(this.shoppingListForm.value);

    shoppingList.items = this.shoppingList.items;

    if (this.isUpdatingShoppingList) {
      if (this.shoppingList.status == 0) {
        this.sls.updateInfo(shoppingList).subscribe(res => {
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/lists', this.shoppingList.id]);
        });
      }
      if (this.shoppingList.status == 1) {
        this.sls.updatePrice(shoppingList).subscribe(res => {
          this.router.routeReuseStrategy.shouldReuseRoute = () => false;
          this.router.onSameUrlNavigation = 'reload';
          this.router.navigate(['/lists', this.shoppingList.id]);
        });
      }
    } else {
      shoppingList.status = 0;
      this.sls.create(shoppingList).subscribe(res => {
        //this.shoppingList = ShoppingListFactory.empty();
        //this.shoppingListForm.reset(ShoppingListFactory.empty());

        this.router.routeReuseStrategy.shouldReuseRoute = () => false;
        this.router.onSameUrlNavigation = 'reload';
        this.router.navigate(['/lists']);

        });
    }
  }

  updateErrorMessages() {
    this.errors = {}
    for (const message of ShoppingListFormErrorMessages) {
      const control = this.shoppingListForm.get(message.forControl);
      if (control && control.dirty && control.invalid &&
        control.errors[message.forValidator] && !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

  getCurrentUserRole() {
    return this.as.getCurrentUserRole();
  }

}
