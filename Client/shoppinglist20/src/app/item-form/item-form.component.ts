import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ItemFormErrorMessages} from "./item-form-error-messages";
import {ShoppingListService} from "../shared/shopping-list.service";
import {AuthService} from "../shared/auth.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Item} from "../shared/item";
import {ItemService} from "../shared/item.service";
import {ItemFactory} from "../shared/item-factory";
import {ShoppingList} from "../shared/shopping-list";

@Component({
  selector: 'bs-item-form',
  templateUrl: './item-form.component.html',
  styles: []
})
export class ItemFormComponent implements OnInit {

  itemForm: FormGroup;
  item = ItemFactory.empty();
  errors: { [key: string]: string } = {};
  shoppingList: ShoppingList;
  listId: number;

  constructor(private sls: ShoppingListService,
              private is: ItemService,
              private as: AuthService,
              private fb: FormBuilder,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    const params = this.route.snapshot.params;
    this.sls.getAllShoppingListsByIdWithUsers(params['listId']).subscribe(res => this.shoppingList = res);
    this.listId = params['listId'];

    this.initItem();
  }

  initItem() {
    this.itemForm = this.fb.group({
      id: this.item.id,
      title: this.item.title,
      description: this.item.description,
      amount: this.item.amount,
      max_price: this.item.max_price
    });
    this.itemForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  submitForm() {
    const item: Item = ItemFactory.fromObject(this.itemForm.value);

    this.is.create(item, this.listId).subscribe(res => {
      //this.router.navigate([], {relativeTo: this.route});
      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/lists', this.listId]);
    });
  }

  updateErrorMessages() {
    this.errors = {}
    for (const message of ItemFormErrorMessages) {
      const control = this.itemForm.get(message.forControl);
      if (control && control.dirty && control.invalid &&
        control.errors[message.forValidator] && !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }
}
