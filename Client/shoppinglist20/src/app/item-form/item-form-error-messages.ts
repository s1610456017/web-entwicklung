export class ErrorMessage {
  constructor(public forControl: string, public forValidator: string, public text: string) {  }
}

export const ItemFormErrorMessages = [
  new ErrorMessage('title', 'required',
    'Der Artikel muss einen Namen haben.'),
  new ErrorMessage('amount', 'required',
    'Bitte die gewünschte Menge des Artikels angeben.')
];
