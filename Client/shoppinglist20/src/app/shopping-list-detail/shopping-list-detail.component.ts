import {Component, OnInit} from '@angular/core';
import {ShoppingListService} from "../shared/shopping-list.service";
import {ShoppingList} from "../shared/shopping-list";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../shared/user.service";
import {User} from "../shared/user";
import {ItemService} from "../shared/item.service";
import {Item} from "../shared/item";
import {AuthService} from "../shared/auth.service";

declare var $:any;

@Component({
  selector: 'bs-shopping-list-detail',
  templateUrl: './shopping-list-detail.component.html',
  styles: []
})
export class ShoppingListDetailComponent implements OnInit {

  shoppingList: ShoppingList;
  user: User;
  items: Item[];

  constructor(
    private sls: ShoppingListService,
    private us: UserService,
    private is: ItemService,
    private as: AuthService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit() {
    const params = this.route.snapshot.params;
    this.sls.getAllShoppingListsByIdWithUsers(params['listId']).subscribe(res => this.shoppingList = res);
  }

  showNewItem(): void {
    $('.ui.modal.newitem')
      .modal('show')
    ;
  }

  showNewList(): void {
    $('.ui.modal.newlist')
      .modal('show')
    ;
  }

  showNewComment(): void {
    $('.ui.modal.newcomment')
      .modal('show')
    ;
  }

  updateHelper() {
    this.shoppingList.helper_id = localStorage['userId'];
    this.sls.updateHelper(this.shoppingList).subscribe(res => {
      //this.shoppingList = ShoppingListFactory.empty();
      //this.shoppingListForm.reset(ShoppingListFactory.empty());

      this.router.routeReuseStrategy.shouldReuseRoute = () => false;
      this.router.onSameUrlNavigation = 'reload';
      this.router.navigate(['/lists', this.shoppingList.id]);
    });
  }

  getCurrentUser() {
    return this.as.getCurrentUserId();
  }

  getCurrentUserRole() {
    return this.as.getCurrentUserRole();
  }

  calculateMaxTotalPrice() {
    //TODO
  }

}
