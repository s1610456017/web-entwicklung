import {Component, OnInit} from '@angular/core';
import {UserService} from "../shared/user.service";
import {ActivatedRoute} from "@angular/router";
import {AuthService} from "../shared/auth.service";

@Component({
  selector: 'bs-account',
  templateUrl: './account.component.html',
  styles: []
})
export class AccountComponent implements OnInit {

  firstName: string
  lastName: string
  role: number
  street: string
  streetNumber: string
  zip: number
  city: string
  country: string

  constructor(
    private us: UserService,
    private route: ActivatedRoute,
    private as: AuthService
  ) { }

  ngOnInit() {
    this.firstName = localStorage['firstName'];
    this.lastName = localStorage['lastName'];
    this.role = localStorage['role'];
    this.street = localStorage['street'];
    this.streetNumber = localStorage['streetNumber'];
    this.zip = localStorage['zip'];
    this.city = localStorage['city'];
    this.country = localStorage['country'];
  }

  isLoggedIn() {
    return this.as.isLoggedIn();
  }

  logout() {
    return this.as.logout();
  }
}
