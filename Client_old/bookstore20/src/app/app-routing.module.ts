import {RouterModule, Routes} from "@angular/router";
import {HomeComponent} from "./home/home.component";
import {ShoppingListsOverviewComponent} from "./shopping-lists-overview/shopping-lists-overview.component";
import {ShoppingListDetailComponent} from "./shopping-list-detail/shopping-list-detail.component";
import {NgModule} from "@angular/core";
import {ShoppingListFormComponent} from "./shopping-list-form/shopping-list-form.component";
import {LoginComponent} from "./login/login.component";

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'lists', component: ShoppingListsOverviewComponent },
  { path: 'lists/:id', component: ShoppingListDetailComponent },
  { path: 'admin', component: ShoppingListFormComponent },
  { path: 'admin/:id', component: ShoppingListFormComponent},
  { path: 'login', component: LoginComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule {}
