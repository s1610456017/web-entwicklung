import { Component, OnInit } from '@angular/core';
import {Feedback, Item, ShoppingList, User} from "../shared/shopping-list";
import {ShoppingListStoreService} from "../shared/shopping-list-store.service";
import {AuthService} from "../shared/authentication.service";

@Component({
  selector: 'bs-home',
  templateUrl: './home.component.html',
  styles: []
})

export class HomeComponent implements OnInit {

  shoppingLists: ShoppingList[];

  constructor(private ss: ShoppingListStoreService, private authService: AuthService) {  }

  ngOnInit() {
    var userId = this.authService.getCurrentUserId();
    var role = this.authService.getCurrentUserRole();
    var firstName = this.authService.getCurrentUserFirstName();

    console.log(userId);
    console.log(role);
    console.log(firstName);
    this.ss.getAllListsByStatus(0).subscribe(res => this.shoppingLists = res);
  }

}
