import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Feedback, Item, ShoppingList, User} from "../shared/shopping-list";
import {ShoppingListStoreService} from "../shared/shopping-list-store.service";
import {AuthService} from "../shared/authentication.service";
import {find} from "rxjs/operators";

@Component({
  selector: 'bs-shopping-lists-overview',
  templateUrl: './shopping-lists-overview.component.html',
  styles: []
})

export class ShoppingListsOverviewComponent implements OnInit {

  shoppingLists: ShoppingList[];

  constructor(private ss: ShoppingListStoreService, private authService: AuthService) {  }

  ngOnInit() {
    var userId = this.authService.getCurrentUserId();
    var role = this.authService.getCurrentUserRole();
    var firstName = this.authService.getCurrentUserFirstName();

    console.log(userId);
    console.log(role);
    console.log(firstName);
    this.ss.getAll(userId, role).subscribe(res => this.shoppingLists = res);
  }

  filterByIdAndStatus($status) {
    this.ss.getAllByIdAndStatus(this.authService, $status).subscribe(res => this.shoppingLists = res);
  }

  filterByAll() {
    this.ss.getAll(this.authService.getCurrentUserId(), this.authService.getCurrentUserRole()).subscribe(res => this.shoppingLists = res);
  }
}
