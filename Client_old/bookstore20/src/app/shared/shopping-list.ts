import {Item} from "./item";
export {Item} from "./item";
import {Feedback} from "./feedback";
export {Feedback} from "./feedback";
import {User} from "./user";
export {User} from "./user";

export class ShoppingList {
  constructor (public id: number,
               public title: string,
               public creator_id: number,
               public date: Date,
               public status: number,
               public description?: string,
               public items?: Item[],
               public feedback?: Feedback[],
               public helper_id?: number,
               public total_price?: number,
               public photo_bill?: string) {}
}
