import { Injectable } from '@angular/core';
import {Feedback, Item, ShoppingList, User} from "./shopping-list";
import {HttpClient} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {catchError, retry} from "rxjs/operators";

@Injectable()

export class ShoppingListStoreService {
  private api = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api';

  constructor(private http: HttpClient) { }

  getAll(userId: number, role: number) : Observable<Array<ShoppingList>> {
    if (role == 0) {
      return this.http.get(`${this.api}/creator-lists/${userId}`)
        .pipe(retry(3)).pipe(catchError(this.errorHandler))
    } else {
      return this.http.get(`${this.api}/helper-lists/${userId}`)
        .pipe(retry(3)).pipe(catchError(this.errorHandler))
    }
  }

  updateHelper(id, helper_id) : Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/list/${id}/${helper_id}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  updateStatus(id, status) : Observable<Array<ShoppingList>> {
    return this.http.get(`${this.api}/list/${id}/${status}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  getAllListsByStatus(status: number) : Observable<Array<ShoppingList>> {
        return this.http.get(`${this.api}/lists/${status}`)
          .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  getAllByIdAndStatus(authService, status: number) : Observable<Array<ShoppingList>> {
    var userId = authService.getCurrentUserId();
    if (authService.getCurrentUserRole() == 0) {
      return this.http.get(`${this.api}/creator-lists/${userId}/${status}`)
        .pipe(retry(3)).pipe(catchError(this.errorHandler))
    } else {
      return this.http.get(`${this.api}/helper-lists/${userId}/${status}`)
        .pipe(retry(3)).pipe(catchError(this.errorHandler))
    }
  }

  getSingle(id: number) : Observable<ShoppingList> {
    return this.http.get<ShoppingList>(`${this.api}/list/${id}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  create(shoppingList: ShoppingList) : Observable<any> {
    return this.http.post(`${this.api}/list`, shoppingList)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  update(shoppingList: ShoppingList) : Observable<any> {
    return this.http.put(`${this.api}/list/${shoppingList.id}`, shoppingList)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  remove(id: number) : Observable<any> {
    return this.http.delete(`${this.api}/list/${id}`)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }

  private errorHandler(error: Error | any): Observable<any> {
    return throwError(error);
  }

  createItem(shoppingListItem: Item, id: number) {
    return this.http.post(`${this.api}/list-add-item/${id}`, shoppingListItem)
      .pipe(retry(3)).pipe(catchError(this.errorHandler))
  }
}
