import {ShoppingList} from "./shopping-list";

export class ShoppingListFactory {
  static empty(): ShoppingList {
    return new ShoppingList(null, null, null, null, null,
                  null, [], [], null, null, null);
  }

  static fromObject(rawShoppingList: any) : ShoppingList {
    return new ShoppingList(
      rawShoppingList.id,
      rawShoppingList.title,
      rawShoppingList.creator_id,
      rawShoppingList.date,
      rawShoppingList.status,
      rawShoppingList.description,
      rawShoppingList.items,
      rawShoppingList.feedback,
      rawShoppingList.helper_id,
      rawShoppingList.total_price,
      rawShoppingList.photo_bill,
    );

  }
}
