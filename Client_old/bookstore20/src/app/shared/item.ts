export class Item {
  constructor (public id: number, public title: string, public description?: string,
               public max_price?: number, public amount?: number) {}
}
