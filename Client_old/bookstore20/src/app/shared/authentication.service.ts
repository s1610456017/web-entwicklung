import { Injectable } from '@angular/core';
import { isNullOrUndefined } from 'util';
import {HttpClient} from "@angular/common/http";
import * as decode from 'jwt-decode';
import {retry} from "rxjs/operators";

interface User {
  result: {
    created_at: Date,
    email: string,
    id: number,
    firstName: string,
    lastName: string,
    role: number,
    updated_at: Date
  }
}

@Injectable()

export class AuthService {
  private api: string = 'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api/auth'; //'http://einkaufshilfe.s1610456017.student.kwmhgb.at/api/login'

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    return this.http.post(`${this.api}/login`, {'email': email, 'password': password});
  }

  /*
  public setCurrentUserId() {
    return this.http.get<User>(`${this.api}/user`).pipe(retry(3)).subscribe(res => {
      localStorage.setItem('userId', res.result.id.toString());
    })
  }
  */

  public getCurrentUserId() {
    return Number.parseInt(localStorage.getItem('userId'));
  }

  public getCurrentUserRole() {
    return Number.parseInt(localStorage.getItem('role'));
  }

  public getCurrentUserFirstName() {
    return localStorage.getItem('firstName');
  }

  public getCurrentUserLastName() {
    return localStorage.getItem('lastName');
  }

  public getCurrentUserAddress() {
    return localStorage.getItem('street');
  }


  public setLocalStorage(token: string) {
    console.log("Storing token");
    console.log(token);
    const decodedToken = decode(token);
    console.log(decodedToken);
    console.log(decodedToken.user.id);
    console.log('---------------------');
    console.log(decodedToken.user);
    console.log('---------------------');
    console.log(decodedToken.user.role);
    localStorage.setItem('token', token);
    localStorage.setItem('userId', decodedToken.user.id);
    localStorage.setItem('role', decodedToken.user.role);
    localStorage.setItem('firstName', decodedToken.user.first_name);
    localStorage.setItem('lastName', decodedToken.user.last_name);
    localStorage.setItem('street', decodedToken.user.address);

  }

  logout() {
    this.http.post(`${this.api}/logout`, {});
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    localStorage.removeItem('role');
    console.log("logged out");
  }

  public isLoggedIn() {
    if (!isNullOrUndefined(localStorage.getItem('token'))) {
      let token : string = localStorage.getItem('token');
      //console.log(token);
      const decodedToken = decode(token);
      let expirationDate: Date = new Date(0);
      expirationDate.setUTCSeconds(decodedToken.exp);
      if (expirationDate < new Date()) {
        console.log("token expired");
        localStorage.removeItem("token");
        return false;
      }
      return true;
    } else {
      return false
    }
  }

  isLoggedOut() {
    return !this.isLoggedIn();
  }
}








