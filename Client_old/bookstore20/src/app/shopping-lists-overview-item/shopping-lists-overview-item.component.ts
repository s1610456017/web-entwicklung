import { Component, OnInit, Input } from '@angular/core';
import {ShoppingList} from "../shared/shopping-list";

@Component({
  selector: 'a.bs-shopping-lists-overview-item',
  templateUrl: './shopping-lists-overview-item.component.html',
  styles: []
})

export class ShoppingListsOverviewItemComponent implements OnInit {
  @Input() shoppingList: ShoppingList;
  constructor() { }

  ngOnInit() {
  }

}
