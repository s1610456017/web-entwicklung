export class ErrorMessage {
  constructor(public forControl: string, public forValidator: string, public text: string) {  }
}

export const ShoppingListDetailErrorMessages = [
  new ErrorMessage('title', 'required',
    'Das Item muss einen Namen haben.'),
  new ErrorMessage('amount', 'required',
    'Das Item muss eine Anzahl haben.')
];
