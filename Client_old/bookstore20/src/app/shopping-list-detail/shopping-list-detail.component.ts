import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ItemFactory} from "../shared/item-factory";
import {ShoppingListStoreService} from "../shared/shopping-list-store.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ShoppingList} from "../shared/shopping-list";
import {ShoppingListDetailErrorMessages} from "./shopping-list-detail-error-messages";
import {AuthService} from "../shared/authentication.service";
import {Item} from "../shared/shopping-list";
import {ShoppingListFactory} from "../shared/shopping-list-factory";
import {ShoppingListFormErrorMessages} from "../shopping-list-form/shopping-list-form-error-messages";

@Component({
  selector: 'bs-shopping-list-detail',
  templateUrl: './shopping-list-detail.component.html',
  styles: []
})

export class ShoppingListDetailComponent implements OnInit {

  shoppingListItemForm: FormGroup;
  shoppingList: ShoppingList = ShoppingListFactory.empty();
  //shoppingList: ShoppingList;
  errors: { [key: string]: string } = {};
  item: Item = ItemFactory.empty();

  constructor(private fb: FormBuilder, private ss: ShoppingListStoreService, private router: Router,
              private route: ActivatedRoute, public authService: AuthService) { }

  ngOnInit() {
    const params = this.route.snapshot.params;
    this.ss.getSingle(params['id']).subscribe(l => this.shoppingList = l);

    this.initShoppingListItem();
  }

  initShoppingListItem() {
    this.shoppingListItemForm = this.fb.group({
      id: this.item.id,
      title: this.item.title,
      description: this.item.description,
      max_price: this.item.max_price,
      amount: this.item.amount
    });
    this.shoppingListItemForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  submitForm() {
    console.log(this.shoppingListItemForm.value);
    const shoppingListItem: Item = ItemFactory.fromObject(this.shoppingListItemForm.value);
    console.log(shoppingListItem);

    this.ss.createItem(shoppingListItem, this.shoppingList.id).subscribe(res => {
      this.item = ItemFactory.empty();
      this.shoppingListItemForm.reset(ItemFactory.empty());
      this.router.navigate(['../lists'], {relativeTo: this.route});
    });
  }

  updateErrorMessages() {
    this.errors = {}
    for (const message of ShoppingListDetailErrorMessages) {
      const control = this.shoppingListItemForm.get(message.forControl);
      if (control && control.dirty && control.invalid &&
        control.errors[message.forValidator] && !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }

  updateHelper($id, $helper_id) {
    //this.ss.updateHelper($id, $helper_id).subscribe(res => this.shoppingList = res);
  }

  updateStatus($id, $status) {
    //this.ss.updateStatus($id, $status).subscribe(res => this.shoppingList = res);
  }

  removeShoppingList() {
    if (confirm('Liste wirklich löschen?')) {
      this.ss.remove(this.shoppingList.id)
        .subscribe(res => this.router.navigate(['../'],
          {relativeTo: this.route }));
    }
  }
}
