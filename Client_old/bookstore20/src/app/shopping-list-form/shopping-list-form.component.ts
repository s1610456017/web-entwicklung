import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {ShoppingListFactory} from "../shared/shopping-list-factory";
import {ShoppingListStoreService} from "../shared/shopping-list-store.service";
import {ActivatedRoute, Router} from "@angular/router";
import {ShoppingList} from "../shared/shopping-list";
import {ShoppingListFormErrorMessages} from "./shopping-list-form-error-messages";
import {AuthService} from "../shared/authentication.service";

@Component({
  selector: 'bs-shopping-list-form',
  templateUrl: './shopping-list-form.component.html',
})

export class ShoppingListFormComponent implements OnInit {

  shoppingListForm: FormGroup;
  shoppingList = ShoppingListFactory.empty();
  errors: { [key: string]: string } = {};
  isUpdatingShoppingList = false;

  constructor(private fb: FormBuilder, private ss: ShoppingListStoreService,
              private route: ActivatedRoute, private router: Router, private authService: AuthService) {
  }

  ngOnInit() {
    if (this.authService.isLoggedIn()) {
      const id = this.route.snapshot.params['id'];
      if (id) {
        this.isUpdatingShoppingList = true;
        this.ss.getSingle(id).subscribe(shoppingList => {
          this.shoppingList = shoppingList;
          this.initShoppingList();
        });
      }
      this.initShoppingList();
    }
    else {
      this.router.navigate(['../login'], {relativeTo: this.route});
    }
  }

  initShoppingList() {
    this.shoppingListForm = this.fb.group({
      id: this.shoppingList.id,
      title: this.shoppingList.title,
      description: this.shoppingList.description,
      creator_id: this.authService.getCurrentUserId(),
      helper_id: this.shoppingList.helper_id,
      date: this.shoppingList.date,
      total_price: this.shoppingList.total_price,
      photo_bill: this.shoppingList.photo_bill,
      status: this.shoppingList.status,
      items: this.shoppingList.items
    });
    this.shoppingListForm.statusChanges.subscribe(() => this.updateErrorMessages());
  }

  submitForm() {
    console.log(this.shoppingListForm.value);
    const shoppingList: ShoppingList = ShoppingListFactory.fromObject(this.shoppingListForm.value);
    console.log(shoppingList);

    shoppingList.items = this.shoppingList.items;

    if (this.isUpdatingShoppingList) {
      this.ss.update(shoppingList).subscribe(res => {
        this.router.navigate(['../../lists', shoppingList.id], {relativeTo: this.route});
      });
    } else {
      shoppingList.status = 0;
      console.log(shoppingList);

      this.ss.create(shoppingList).subscribe(res => {
        this.shoppingList = ShoppingListFactory.empty();
        this.shoppingListForm.reset(ShoppingListFactory.empty());
        this.router.navigate(['../lists'], {relativeTo: this.route});
      });
    }
  }

  updateErrorMessages() {
    this.errors = {}
    for (const message of ShoppingListFormErrorMessages) {
      const control = this.shoppingListForm.get(message.forControl);
      if (control && control.dirty && control.invalid &&
        control.errors[message.forValidator] && !this.errors[message.forControl]) {
        this.errors[message.forControl] = message.text;
      }
    }
  }
}
