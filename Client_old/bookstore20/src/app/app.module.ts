import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ShoppingListsOverviewComponent } from './shopping-lists-overview/shopping-lists-overview.component';
import { ShoppingListsOverviewItemComponent } from './shopping-lists-overview-item/shopping-lists-overview-item.component';
import { ShoppingListDetailComponent } from './shopping-list-detail/shopping-list-detail.component';
import {ShoppingListStoreService} from "./shared/shopping-list-store.service";
import { HomeComponent } from './home/home.component';
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { ShoppingListFormComponent } from './shopping-list-form/shopping-list-form.component';
import {ReactiveFormsModule} from "@angular/forms";
import { LoginComponent } from './login/login.component';
import {AuthService} from "./shared/authentication.service";
import {TokenInterceptorService} from "./shared/token-interceptor.service";
import {JwtInterceptorService} from "./shared/jwt-interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
    ShoppingListsOverviewComponent,
    ShoppingListsOverviewItemComponent,
    ShoppingListDetailComponent,
    HomeComponent,
    ShoppingListFormComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule, AppRoutingModule, HttpClientModule, ReactiveFormsModule
  ],
  providers: [ShoppingListStoreService, AuthService,
    {provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorService,
      multi: true
  }, {provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptorService,
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
